"use strict";
exports.__esModule = true;
var base_ships_1 = require("./base-ships");
var star_figthers_1 = require("./star-figthers");
var _ = require("lodash");
var execute = function () {
    lessonThree();
};
var lessonOne = function () {
    var message = "May the force be with you!";
    console.log("Obi-Wan Kenobi. ", message);
    var episode = 4;
    console.log("This is episode ", episode);
    episode = episode + 1;
    console.log("Next episode is ", episode);
    var favoriteDroid;
    favoriteDroid = "BB-8";
    console.log("My favorite droid is ", favoriteDroid);
    var isEnoughToBeatMF = function (parsecs) {
        return parsecs < 12;
    };
    var distance = 14;
    console.log("Is " + distance + " parsecs enough to beat Millenium Falcon? " + (isEnoughToBeatMF(distance) ? "YES" : "NO"));
    var call = function (name) { return console.log("Do you copy, " + name + "?"); };
    call("R2");
    function inc(speed, inc) {
        if (inc === void 0) { inc = 1; }
        return speed + inc;
    }
    console.log("inc (5, 1) = " + inc(5, 1));
    console.log("inc (5) = " + inc(5));
};
var lessonTwo = function () {
    var ship = new base_ships_1.SpaceCraft("hyperdrive");
    ship.jumpIntoHyperSpace();
    var millenniumFalcon = new star_figthers_1.MillenniumFalcon();
    millenniumFalcon.jumpIntoHyperSpace();
    var goodForTheJob = function (ship) { return ship.cargoContainers > 2; };
    console.log("Is falcon good for the job? " + (goodForTheJob(millenniumFalcon) ? 'yes' : 'no'));
};
var lessonThree = function () {
    console.log(_.pad("Typescript Examples", 40, "="));
};
//---------------------------------------------EXECUTION---------------------------------------------
execute();
