interface ContainerShip {
    cargoContainers: number;
}

class SpaceCraft {
    constructor(public propulsor: string) { }

    jumpIntoHyperSpace() {
        console.log(`Entering hyperspace with ${this.propulsor}`);
    }
}

export { SpaceCraft, ContainerShip };