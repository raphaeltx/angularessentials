import { SpaceCraft, ContainerShip } from './base-ships';
import { MillenniumFalcon } from './star-figthers';
import * as _ from 'lodash';

let execute = () => {
    lessonThree();
}

let lessonOne = () => {
    let message: string = "May the force be with you!";

    console.log("Obi-Wan Kenobi. ", message);

    let episode: number = 4;

    console.log("This is episode ", episode);

    episode = episode + 1;

    console.log("Next episode is ", episode);

    let favoriteDroid: string;

    favoriteDroid = "BB-8";

    console.log("My favorite droid is ", favoriteDroid);

    let isEnoughToBeatMF = function (parsecs: number): boolean {
        return parsecs < 12;
    }

    let distance = 14;

    console.log(`Is ${distance} parsecs enough to beat Millenium Falcon? ${isEnoughToBeatMF(distance) ? "YES" : "NO"}`);

    let call = (name: string) => console.log(`Do you copy, ${name}?`);

    call("R2");

    function inc(speed: number, inc: number = 1): number {
        return speed + inc;
    }

    console.log(`inc (5, 1) = ${inc(5, 1)}`);
    console.log(`inc (5) = ${inc(5)}`);
}

let lessonTwo = () => {
    let ship = new SpaceCraft("hyperdrive");
    ship.jumpIntoHyperSpace();

    let millenniumFalcon = new MillenniumFalcon();
    millenniumFalcon.jumpIntoHyperSpace();

    let goodForTheJob = (ship: ContainerShip) => ship.cargoContainers > 2

    console.log(`Is falcon good for the job? ${goodForTheJob(millenniumFalcon) ? 'yes' : 'no'}`);
}

let lessonThree = () => {
    console.log(_.pad("Typescript Examples", 40, "="));
}

//---------------------------------------------EXECUTION---------------------------------------------

execute();
